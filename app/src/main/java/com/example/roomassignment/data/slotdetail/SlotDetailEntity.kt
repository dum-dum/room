package com.example.roomassignment.data.slotdetail

import androidx.room.*
import com.example.roomassignment.data.employee.EmployeeEntity
import com.example.roomassignment.data.slotmaster.SlotMasterEntity
import com.example.roomassignment.model.SlotDetailModel

@Entity(
    tableName = "slot_detail",
    foreignKeys = [ForeignKey(
        entity = SlotMasterEntity::class,
        parentColumns = ["id"],
        childColumns = ["slot_id"]
    ),
        ForeignKey(
            entity = EmployeeEntity::class,
            parentColumns = ["id"],
            childColumns = ["emp_id"]
        )]
)
data class SlotDetailEntity constructor(
    @ColumnInfo(name = "slot_id") var slotId: Int,
    @ColumnInfo(name = "emp_id") var empId: Int,
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var slotDetailId: Int? = null
) {
    fun mapToViewModel() = SlotDetailModel(slotDetailId ?: -1, slotId, empId)
}

data class SlotDetailSlotMasterEmployeeJoin(@Embedded var slotDetailEntity: SlotDetailEntity) {

    @Relation(entity = SlotMasterEntity::class, parentColumn = "slot_id", entityColumn = "id")
    var slots: List<SlotMasterEntity> = mutableListOf()

    @Relation(entity = EmployeeEntity::class, parentColumn = "emp_id", entityColumn = "id")
    var employees: List<EmployeeEntity> = mutableListOf()

    fun mapToViewModel() = SlotDetailModel(
        slotDetailEntity.slotDetailId ?: -1,
        slotDetailEntity.slotId,
        slotDetailEntity.empId
    ).apply {
        slotMaster = this@SlotDetailSlotMasterEmployeeJoin.slots.map {
            it.mapToViewModel()
        }
        employee = this@SlotDetailSlotMasterEmployeeJoin.employees.map {
            it.mapToViewModel()
        }
    }


}