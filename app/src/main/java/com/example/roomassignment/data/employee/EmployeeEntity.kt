package com.example.roomassignment.data.employee

import androidx.room.*
import com.example.roomassignment.data.slotdetail.SlotDetailEntity
import com.example.roomassignment.model.EmployeeModel

@Entity(tableName = "employee")
data class EmployeeEntity constructor(

    @ColumnInfo(name = "name") var name: String,
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var id: Int? = null
) {
    fun mapToViewModel() = EmployeeModel(id ?: -1, name)
}

data class EmployeeSlotDetailJoin(@Embedded var employeeEntity: EmployeeEntity) {
    @Relation(entity = SlotDetailEntity::class, parentColumn = "id", entityColumn = "emp_id")
    var slotDetail: List<SlotDetailEntity> = mutableListOf()

    fun mapToViewModel() = EmployeeModel(employeeEntity.id ?: -1, employeeEntity.name).apply {
        slotDetail = this@EmployeeSlotDetailJoin.slotDetail.map {
            it.mapToViewModel()
        }
    }

}