package com.example.roomassignment.data.employee

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData

class EmployeeRepositoryIMPL(private val employeeDao: EmployeeDao) : EmployeeRepository {

    @WorkerThread
    override fun insert(employeeEntity: EmployeeEntity): Long {
        return employeeDao.insert(employeeEntity)
    }

    @WorkerThread
    override fun insert(employeeEntityList: List<EmployeeEntity>): List<Long> {
        return employeeDao.insert(employeeEntityList)
    }

    @WorkerThread
    override fun update(employeeEntity: EmployeeEntity): Int {
        return employeeDao.update(employeeEntity)
    }

    @WorkerThread
    override fun delete(employeeEntity: EmployeeEntity): Int {
        return employeeDao.delete(employeeEntity)
    }

    @WorkerThread
    override fun nukeEmployees(): Int {
        return employeeDao.deleteAll()
    }

    @WorkerThread
    override fun getEmployees(): LiveData<List<EmployeeSlotDetailJoin>> {
        return employeeDao.getAll()
    }

}