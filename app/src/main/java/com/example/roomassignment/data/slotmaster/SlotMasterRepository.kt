package com.example.roomassignment.data.slotmaster

import androidx.lifecycle.LiveData

interface SlotMasterRepository {
    fun insert(slotMasterEntity: SlotMasterEntity): Long
    fun insert(slotMasterEntityList: List<SlotMasterEntity>): List<Long>
    fun update(slotMasterEntity: SlotMasterEntity): Int
    fun delete(slotMasterEntity: SlotMasterEntity): Int
    fun nukeSlots(): Int
    fun getSlots(): LiveData<List<SlotMasterSlotDetailJoin>>
    fun getSlotsByRoomId(id: Int): LiveData<List<SlotMasterSlotDetailJoin>>
}