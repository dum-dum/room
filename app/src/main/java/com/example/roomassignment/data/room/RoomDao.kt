package com.example.roomassignment.data.room

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface RoomDao {

    @Transaction
    @Query("select * from room")
    fun getAll(): LiveData<List<RoomSlotJoin>>

    @Query("select * from room where id=:id")
    fun getUserById(id: Int): LiveData<List<RoomSlotJoin>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(roomEntity: RoomEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(roomEntityList: List<RoomEntity>): List<Long>

    @Update
    fun update(roomEntity: RoomEntity): Int

    @Delete
    fun delete(roomEntity: RoomEntity): Int

    @Query("DELETE from room")
    fun deleteAll(): Int

    @Query("delete from room where id=:roomId")
    fun deleteWithRoomId(roomId: Int): Int

}
