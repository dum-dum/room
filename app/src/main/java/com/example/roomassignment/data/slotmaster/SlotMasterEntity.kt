package com.example.roomassignment.data.slotmaster

import androidx.room.*
import com.example.roomassignment.data.room.RoomEntity
import com.example.roomassignment.data.slotdetail.SlotDetailEntity
import com.example.roomassignment.model.SlotMasterModel
import org.joda.time.LocalTime

@Entity(
    tableName = "slot_master",
    foreignKeys = [ForeignKey(
        entity = RoomEntity::class,
        parentColumns = ["id"],
        childColumns = ["room_id"], onDelete = ForeignKey.CASCADE
    )]
)
data class SlotMasterEntity constructor(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var id: Int? = null,
    @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "start_time") var startTime: LocalTime,
    @ColumnInfo(name = "end_time") var endTime: LocalTime,
    @ColumnInfo(name = "room_id") var roomId: Int
) {
    fun mapToViewModel() = SlotMasterModel(id ?: -1, name, startTime, endTime, roomId)
}

data class SlotMasterSlotDetailJoin(@Embedded var slotMasterEntity: SlotMasterEntity) {
    @Relation(entity = SlotDetailEntity::class, parentColumn = "id", entityColumn = "slot_id")
    var slotDetail = mutableListOf<SlotDetailEntity>()

    fun mapToViewModel() = SlotMasterModel(
        slotMasterEntity.id ?: -1,
        slotMasterEntity.name,
        slotMasterEntity.startTime,
        slotMasterEntity.endTime,
        slotMasterEntity.roomId
    ).apply {
        slotDetail = this@SlotMasterSlotDetailJoin.slotDetail.map {
            it.mapToViewModel()
        }
    }

//    @Relation(entity = RoomEntity::class,parentColumn = "room_id",entityColumn = "id")
//    var room = mutableListOf<RoomEntity>()
}