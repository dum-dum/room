package com.example.roomassignment.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.roomassignment.data.employee.EmployeeDao
import com.example.roomassignment.data.employee.EmployeeEntity
import com.example.roomassignment.data.room.RoomDao
import com.example.roomassignment.data.room.RoomEntity
import com.example.roomassignment.data.slotdetail.SlotDetailDao
import com.example.roomassignment.data.slotdetail.SlotDetailEntity
import com.example.roomassignment.data.slotmaster.SlotMasterDao
import com.example.roomassignment.data.slotmaster.SlotMasterEntity
import com.example.roomassignment.ui.utils.TypeConverters

@androidx.room.TypeConverters(TypeConverters::class)
@Database(
    entities = [RoomEntity::class, EmployeeEntity::class, SlotMasterEntity::class, SlotDetailEntity::class],
    version = 2
)
abstract class ApplicationDatabase : RoomDatabase() {
    abstract val roomDao: RoomDao
    abstract val employeeDao: EmployeeDao
    abstract val slotMasterDao: SlotMasterDao
    abstract val slotDetailDao: SlotDetailDao
}