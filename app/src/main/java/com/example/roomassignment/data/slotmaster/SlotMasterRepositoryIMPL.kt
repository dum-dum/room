package com.example.roomassignment.data.slotmaster

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData

class SlotMasterRepositoryIMPL(private val slotMasterDao: SlotMasterDao) : SlotMasterRepository {

    @WorkerThread
    override fun insert(slotMasterEntity: SlotMasterEntity): Long {
        return slotMasterDao.insert(slotMasterEntity)
    }

    @WorkerThread
    override fun insert(slotMasterEntityList: List<SlotMasterEntity>): List<Long> {
        return slotMasterDao.insert(slotMasterEntityList)
    }

    @WorkerThread
    override fun update(slotMasterEntity: SlotMasterEntity): Int {
        return slotMasterDao.update(slotMasterEntity)
    }

    @WorkerThread
    override fun delete(slotMasterEntity: SlotMasterEntity): Int {
        return slotMasterDao.delete(slotMasterEntity)
    }

    @WorkerThread
    override fun nukeSlots(): Int {
        return slotMasterDao.deleteAll()
    }

    @WorkerThread
    override fun getSlots(): LiveData<List<SlotMasterSlotDetailJoin>> {
        return slotMasterDao.getAll()
    }

    @WorkerThread
    override fun getSlotsByRoomId(id: Int): LiveData<List<SlotMasterSlotDetailJoin>> {
        return slotMasterDao.getSlotsByRoomId(id)
    }
}