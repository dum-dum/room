package com.example.roomassignment.data.room

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData

class RoomRepositoryIMPL(private val roomDao: RoomDao) : RoomRepository {

    @WorkerThread
    override fun insert(roomEntity: RoomEntity): Long {
        return roomDao.insert(roomEntity)
    }

    @WorkerThread
    override fun insert(roomEntityList: List<RoomEntity>): List<Long> {
        return roomDao.insert(roomEntityList)
    }

    @WorkerThread
    override fun update(roomEntity: RoomEntity): Int {
        return roomDao.update(roomEntity)
    }

    @WorkerThread
    override fun delete(roomEntity: RoomEntity): Int {
        return roomDao.delete(roomEntity)
    }

    @WorkerThread
    override fun nukeRooms(): Int {
        return roomDao.deleteAll()
    }

    @WorkerThread
    override fun getRooms(): LiveData<List<RoomSlotJoin>> {
        return roomDao.getAll()
    }

    @WorkerThread
    override fun getRoomById(id: Int): LiveData<List<RoomSlotJoin>> {
        return roomDao.getUserById(id)
    }

    @WorkerThread
    override fun deleteRoomById(roomId: Int): Int {
        return roomDao.deleteWithRoomId(roomId)
    }


}