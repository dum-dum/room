package com.example.roomassignment.data.room

import androidx.lifecycle.LiveData

interface RoomRepository {
    fun insert(roomEntity: RoomEntity): Long
    fun insert(roomEntityList: List<RoomEntity>): List<Long>
    fun update(roomEntity: RoomEntity): Int
    fun delete(roomEntity: RoomEntity): Int
    fun nukeRooms(): Int
    fun getRooms(): LiveData<List<RoomSlotJoin>>
    fun getRoomById(id: Int): LiveData<List<RoomSlotJoin>>
    fun deleteRoomById(roomId: Int): Int
}