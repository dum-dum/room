package com.example.roomassignment.data.slotdetail

import androidx.lifecycle.LiveData

interface SlotDetailRepository {
    fun insert(slotDetailEntity: SlotDetailEntity): Long
    fun insert(slotDetailEntityList: List<SlotDetailEntity>): List<Long>
    fun update(slotDetailEntity: SlotDetailEntity): Int
    fun delete(slotDetailEntity: SlotDetailEntity): Int
    fun nukeSlotsDetail(): Int
    fun getSlotsDetail(): LiveData<List<SlotDetailSlotMasterEmployeeJoin>>
    fun getSlotsDetailWithSlotId(slotId: Int): LiveData<List<SlotDetailSlotMasterEmployeeJoin>>
    fun deleteWithSlotId(slotId: Int): Int
    fun deleteWithEmpId(empId: Int): Int
}