package com.example.roomassignment.data.employee

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface EmployeeDao {

    @Query("select * from employee")
    fun getAll(): LiveData<List<EmployeeSlotDetailJoin>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(employeeEntity: EmployeeEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(employeeEntityList: List<EmployeeEntity>): List<Long>

    @Update
    fun update(employeeEntity: EmployeeEntity): Int

    @Delete
    fun delete(employeeEntity: EmployeeEntity): Int

    @Query("DELETE from employee")
    fun deleteAll(): Int
}