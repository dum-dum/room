package com.example.roomassignment.data.employee

import androidx.lifecycle.LiveData

interface EmployeeRepository {
    fun insert(employeeEntity: EmployeeEntity): Long
    fun insert(employeeEntityList: List<EmployeeEntity>): List<Long>
    fun update(employeeEntity: EmployeeEntity): Int
    fun delete(employeeEntity: EmployeeEntity): Int
    fun nukeEmployees(): Int
    fun getEmployees(): LiveData<List<EmployeeSlotDetailJoin>>
}