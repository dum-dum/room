package com.example.roomassignment.data.slotdetail

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface SlotDetailDao {
    @Query("select * from slot_detail")
    fun getAll(): LiveData<List<SlotDetailSlotMasterEmployeeJoin>>

    @Query("select * from slot_detail where slot_id=:slotId")
    fun getSlotDetailWithSlotId(slotId: Int): LiveData<List<SlotDetailSlotMasterEmployeeJoin>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(slotDetailEntity: SlotDetailEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(slotDetailEntityList: List<SlotDetailEntity>): List<Long>

    @Update
    fun update(slotDetailEntity: SlotDetailEntity): Int

    @Delete
    fun delete(slotDetailEntity: SlotDetailEntity): Int

    @Query("DELETE from slot_detail")
    fun deleteAll(): Int

    @Query("delete from slot_detail where slot_id=:slotId")
    fun deleteWithSlotId(slotId: Int): Int

    @Query("delete from slot_detail where emp_id=:empId")
    fun deleteWithEmpId(empId: Int): Int
}