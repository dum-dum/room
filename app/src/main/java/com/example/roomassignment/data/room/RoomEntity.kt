package com.example.roomassignment.data.room

import androidx.room.*
import com.example.roomassignment.data.slotmaster.SlotMasterEntity
import com.example.roomassignment.data.slotmaster.SlotMasterSlotDetailJoin

@Entity(tableName = "room")
data class RoomEntity constructor(

    @ColumnInfo(name = "name") var name: String,
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var id: Int? = null
)

data class RoomSlotJoin(@Embedded var roomEntity: RoomEntity) {
    @Relation(entity = SlotMasterEntity::class, parentColumn = "id", entityColumn = "room_id")
    var slots = mutableListOf<SlotMasterSlotDetailJoin>()
}