package com.example.roomassignment.data.slotdetail

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData

class SlotDetailRepositoryIMPL(private val slotDetailDao: SlotDetailDao) : SlotDetailRepository {

    @WorkerThread
    override fun insert(slotDetailEntity: SlotDetailEntity): Long {
        return slotDetailDao.insert(slotDetailEntity)
    }

    @WorkerThread
    override fun insert(slotDetailEntityList: List<SlotDetailEntity>): List<Long> {
        return slotDetailDao.insert(slotDetailEntityList)
    }

    @WorkerThread
    override fun update(slotDetailEntity: SlotDetailEntity): Int {
        return slotDetailDao.update(slotDetailEntity)
    }

    @WorkerThread
    override fun delete(slotDetailEntity: SlotDetailEntity): Int {
        return slotDetailDao.delete(slotDetailEntity)
    }

    @WorkerThread
    override fun nukeSlotsDetail(): Int {
        return slotDetailDao.deleteAll()
    }

    @WorkerThread
    override fun getSlotsDetail(): LiveData<List<SlotDetailSlotMasterEmployeeJoin>> {
        return slotDetailDao.getAll()
    }

    @WorkerThread
    override fun getSlotsDetailWithSlotId(slotId: Int): LiveData<List<SlotDetailSlotMasterEmployeeJoin>> {
        return slotDetailDao.getSlotDetailWithSlotId(slotId)
    }

    @WorkerThread
    override fun deleteWithSlotId(slotId: Int): Int {
        return slotDetailDao.deleteWithSlotId(slotId)
    }

    @WorkerThread
    override fun deleteWithEmpId(empId: Int): Int {
        return slotDetailDao.deleteWithEmpId(empId)
    }

}