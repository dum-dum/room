package com.example.roomassignment.data.slotmaster

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface SlotMasterDao {

    @Query("select * from slot_master")
    fun getAll(): LiveData<List<SlotMasterSlotDetailJoin>>

    @Query("select * from slot_master where room_id=:id")
    fun getSlotsByRoomId(id: Int): LiveData<List<SlotMasterSlotDetailJoin>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(slotMasterEntity: SlotMasterEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(slotMasterEntityList: List<SlotMasterEntity>): List<Long>

    @Update
    fun update(slotMasterEntity: SlotMasterEntity): Int

    @Delete
    fun delete(slotMasterEntity: SlotMasterEntity): Int

    @Query("DELETE from slot_master")
    fun deleteAll(): Int
}