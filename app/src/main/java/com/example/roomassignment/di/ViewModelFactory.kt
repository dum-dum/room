package com.example.roomassignment.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.roomassignment.ui.viewmodel.EmployeeViewModel
import com.example.roomassignment.ui.viewmodel.RoomViewModel
import com.example.roomassignment.ui.viewmodel.SlotEmployeeViewModel
import com.example.roomassignment.ui.viewmodel.SlotViewModel
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton
import kotlin.reflect.KClass

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(RoomViewModel::class)
    internal abstract fun roomViewModel(viewModel: RoomViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EmployeeViewModel::class)
    internal abstract fun employeeViewModel(viewModel: EmployeeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SlotViewModel::class)
    internal abstract fun slotViewModel(viewModel: SlotViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SlotEmployeeViewModel::class)
    internal abstract fun slotEmployeeViewModel(viewModel: SlotEmployeeViewModel): ViewModel

    //Add more ViewModels here
}

@Singleton
class ViewModelFactory @Inject constructor(
    private val viewModels: MutableMap<Class<out ViewModel>,
            Provider<ViewModel>>
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        viewModels[modelClass]?.get() as T
}

@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@MapKey
internal annotation class ViewModelKey(val value: KClass<out ViewModel>)
