package com.example.roomassignment.di

import com.example.roomassignment.Application
import dagger.Module
import dagger.Provides
import javax.inject.Qualifier

@Module
class AppModule {

    @ApplicationContext
    @Provides
    fun applicationContext(application: Application) = application.applicationContext
}

@Qualifier
annotation class ApplicationContext
