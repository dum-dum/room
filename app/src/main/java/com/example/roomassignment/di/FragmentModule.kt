package com.example.roomassignment.di

import com.example.roomassignment.ui.fragment.EmployeeFragment
import com.example.roomassignment.ui.fragment.RoomFragment
import com.example.roomassignment.ui.fragment.SlotEmployeeFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeRoomFragment(): RoomFragment

    @ContributesAndroidInjector
    abstract fun contributeEmployeeFragment(): EmployeeFragment

    @ContributesAndroidInjector
    abstract fun contributeSlotEmployeeFragment(): SlotEmployeeFragment

}
