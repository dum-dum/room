package com.example.roomassignment.di

import com.example.roomassignment.ui.activity.MainActivity
import com.example.roomassignment.ui.activity.SelectEmployeesActivity
import com.example.roomassignment.ui.activity.SlotActivity
import com.example.roomassignment.ui.activity.SlotDetailActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {
    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun contributeSlotsActivity(): SlotActivity

    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun contrubuteSlotDetailActivity(): SlotDetailActivity

    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun contributeSelectEmployeeActivity(): SelectEmployeesActivity
}