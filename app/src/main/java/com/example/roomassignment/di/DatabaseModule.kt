package com.example.roomassignment.di

import android.content.Context
import androidx.room.Room
import com.example.roomassignment.data.ApplicationDatabase
import com.example.roomassignment.data.employee.EmployeeDao
import com.example.roomassignment.data.employee.EmployeeRepository
import com.example.roomassignment.data.employee.EmployeeRepositoryIMPL
import com.example.roomassignment.data.room.RoomDao
import com.example.roomassignment.data.room.RoomRepository
import com.example.roomassignment.data.room.RoomRepositoryIMPL
import com.example.roomassignment.data.slotdetail.SlotDetailDao
import com.example.roomassignment.data.slotdetail.SlotDetailRepository
import com.example.roomassignment.data.slotdetail.SlotDetailRepositoryIMPL
import com.example.roomassignment.data.slotmaster.SlotMasterDao
import com.example.roomassignment.data.slotmaster.SlotMasterRepository
import com.example.roomassignment.data.slotmaster.SlotMasterRepositoryIMPL
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {


    // Database Provider
    @Singleton
    @Provides
    fun provideAppDatabase(@ApplicationContext context: Context): ApplicationDatabase {
        return Room.databaseBuilder(context, ApplicationDatabase::class.java, "Database")
            .fallbackToDestructiveMigration()
            .build()
    }

    // Dao Providers
    @Provides
    fun provideRoomDao(applicationDatabase: ApplicationDatabase): RoomDao {
        return applicationDatabase.roomDao
    }

    @Provides
    fun provideEmployeeDao(applicationDatabase: ApplicationDatabase): EmployeeDao {
        return applicationDatabase.employeeDao
    }

    @Provides
    fun provideSlotMasterDao(applicationDatabase: ApplicationDatabase): SlotMasterDao {
        return applicationDatabase.slotMasterDao
    }

    @Provides
    fun provideSlotDetailDao(applicationDatabase: ApplicationDatabase): SlotDetailDao {
        return applicationDatabase.slotDetailDao
    }

    // Repositories Providers
    @Provides
    fun provideRoomRepository(roomDao: RoomDao): RoomRepository {
        return RoomRepositoryIMPL(roomDao)
    }

    @Provides
    fun provideEmployeeRepository(employeeDao: EmployeeDao): EmployeeRepository {
        return EmployeeRepositoryIMPL(employeeDao)
    }

    @Provides
    fun provideSlotMasterRepository(slotMasterDao: SlotMasterDao): SlotMasterRepository {
        return SlotMasterRepositoryIMPL(slotMasterDao)
    }

    @Provides
    fun provideSlotDetailRepository(slotDetailDao: SlotDetailDao): SlotDetailRepository {
        return SlotDetailRepositoryIMPL(slotDetailDao)
    }
}