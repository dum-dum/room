package com.example.roomassignment.model

import org.joda.time.LocalTime


data class RoomModel constructor(
    var id: Int,
    var name: String,
    var slots: List<SlotMasterModel>
)

data class SlotModel constructor(
    var id: Int,
    var name: String
)

data class EmployeeModel constructor(
    var id: Int,
    var name: String
) {
    var isSelected = false
    var slotDetail: List<SlotDetailModel> = listOf()
}

data class SlotMasterModel constructor(
    var id: Int,
    var name: String,
    var startTime: LocalTime,
    var endTime: LocalTime,
    var roomId: Int
) {
    var slotDetail: List<SlotDetailModel> = listOf()
//    var room: List<RoomModel> = listOf()
}


data class SlotDetailModel constructor(
    var id: Int,
    var slotId: Int,
    var empId: Int
) {
    var slotMaster: List<SlotMasterModel> = listOf()
    var employee: List<EmployeeModel> = listOf()
}