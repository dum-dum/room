package com.example.roomassignment.ui.core


import android.graphics.drawable.Drawable
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.IdRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.roomassignment.R
import dagger.android.support.DaggerFragment

open class BaseFragment : DaggerFragment() {

    fun addFragment(@IdRes containerId: Int, fragment: Fragment, addToBackStack: Boolean = true) {
        childFragmentManager.beginTransaction().apply {
            if (addToBackStack) {
                addToBackStack(fragment.javaClass.simpleName)
            }
            add(containerId, fragment)
            commit()
        }
    }

    //    fun replaceFragment(
//        @IdRes containerId: Int, fragment: Fragment, addToBackStack: Boolean = true,
//        animate: Boolean = false, tag: String = ""
//    ) {
//        if (addToBackStack) {
//            childFragmentManager.beginTransaction().apply {
//                if (addToBackStack) {
//                    addToBackStack(fragment.javaClass.simpleName)
//                }
//                if (animate) {
//                    setCustomAnimations(R.anim.slide_in, R.anim.slide_out, R.anim.slide_in, R.anim.slide_out)
//                }
//                replace(containerId, fragment, tag)
//                commit()
//            }
//        }
//    }
    fun replaceFragment(
        @IdRes containerId: Int,
        fragment: Fragment,
        addToBackStack: Boolean = true,
        animate: Boolean = true,
        tag: String = ""
    ) {
        fragmentManager?.beginTransaction()?.apply {
            if (addToBackStack) {
                this.addToBackStack(fragment.javaClass.simpleName)
            }
            if (animate) {
                this.setCustomAnimations(
                    R.anim.slide_in,
                    R.anim.slide_out,
                    R.anim.slide_in,
                    R.anim.slide_out
                )
            }
            replace(containerId, fragment, tag)
            commit()
        }
    }
//        val transaction = fragmentManager?.beginTransaction()
//        transaction?.replace(R.id.fragment_container, fragment)
//        transaction?.addToBackStack("")
//        transaction?.commit()

    fun getDrawable(@DrawableRes drawableId: Int): Drawable? {
        return ContextCompat.getDrawable(activity!!, drawableId)
    }

    fun getColor(@ColorRes colorId: Int): Int {
        return ContextCompat.getColor(activity!!, colorId)
    }

}