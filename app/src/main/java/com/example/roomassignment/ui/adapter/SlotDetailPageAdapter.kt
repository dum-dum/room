package com.example.roomassignment.ui.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class SlotDetailPageAdapter(fm: FragmentManager?, private val arrayOfFragment: Array<Fragment>) :
    FragmentPagerAdapter(fm) {


    override fun getItem(position: Int): Fragment? {
        return when (position) {
            0 -> {
                arrayOfFragment[0]
            }
            1 -> {
                arrayOfFragment[1]
            }
            else -> null
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> {
                "Employee"
            }
            1 -> {
                "About"
            }
            else -> null
        }
    }

    override fun getCount(): Int = 2

}