package com.example.roomassignment.ui.fragment

import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.roomassignment.R
import com.example.roomassignment.ui.adapter.RoomAdapter
import com.example.roomassignment.ui.core.BaseFragment
import com.example.roomassignment.ui.viewmodel.RoomViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.add_room_dialog.view.*
import javax.inject.Inject


class RoomFragment : BaseFragment() {

    private val TAG = javaClass.simpleName

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var roomViewModel: RoomViewModel
    private lateinit var roomAdapter: RoomAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var dialogView: View
    private lateinit var builder: AlertDialog.Builder
    private lateinit var fab: FloatingActionButton


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        roomViewModel = ViewModelProviders.of(this, viewModelFactory)[RoomViewModel::class.java]
        roomAdapter = RoomAdapter(arrayListOf())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        val rootView = inflater.inflate(R.layout.fragment_room, container, false)
        recyclerView = rootView.findViewById(R.id.recyclerview_room)
        roomViewModel.getData()
        attachObserver()
        setupRecyclerView()

        setHasOptionsMenu(true)


        fab = rootView.findViewById(R.id.fab)
        fab.setOnClickListener {
            addRoom()
        }



        return rootView
    }

    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        val inflater = activity!!.menuInflater
        inflater.inflate(R.menu.activity_slot_menu, menu)
        menu.setHeaderTitle("Select The Action")
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        when {
            item.itemId == R.id.action_remove_room -> {
                Toast.makeText(context, "calling code", Toast.LENGTH_LONG).show()
            }
            else -> return false
        }
        return true
    }

    private fun attachObserver() {
        roomViewModel.roomLiveData.observe(this, Observer {
            roomAdapter.updateList(it)
        })
    }

    private fun setupRecyclerView() {
        recyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.adapter = roomAdapter
    }

    private fun addRoom() {
        dialogView = LayoutInflater.from(activity).inflate(R.layout.add_room_dialog, null)
        builder = AlertDialog.Builder(activity!!)
            .setView(dialogView)
            .setTitle("Add Room")
        val alertDialog = builder.show()
        dialogView.button_add_room.setOnClickListener {
            Log.d(TAG, dialogView.dialog_room_name.text.toString())
            roomViewModel.insertData(dialogView.dialog_room_name.text.toString())
            alertDialog.dismiss()
        }
    }

//    private fun recyclerViewTouchListener() {
//        recyclerView.addOnItemTouchListener(
//            RecyclerItemClickListener(activity!!,
//                object : RecyclerItemClickListener.OnItemClickListener {
//                    override fun onItemClick(view: View, position: Int) {
////                        Toast.makeText(activity!!, "OnItemTouchListener $position", Toast.LENGTH_SHORT)
////                            .show()
//                        replaceFragment(R.id.fragment_container,roomSlotsFragment)
//
//                    }
//                })
//        )
//    }

}
