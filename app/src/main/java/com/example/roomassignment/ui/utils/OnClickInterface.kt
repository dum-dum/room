package com.example.roomassignment.ui.utils

import android.view.View

interface OnClickInterface {
    fun onClickListener(v: View, position: Int)
}