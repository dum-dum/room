package com.example.roomassignment.ui.adapter

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.roomassignment.R
import com.example.roomassignment.model.RoomModel
import com.example.roomassignment.ui.activity.SlotActivity
import com.example.roomassignment.ui.utils.ContextAppbarCallback
import com.example.roomassignment.ui.utils.OnClickInterface
import kotlinx.android.synthetic.main.room_list_row.view.*

class RoomAdapter(private var roomList: List<RoomModel>) : RecyclerView.Adapter<RoomViewHolder>(),
    OnClickInterface {
    private val TAG = javaClass.simpleName

    override fun onClickListener(v: View, position: Int) {
        v.setOnClickListener {
            Log.e("CUSTOM TAG", "$position")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoomViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.room_list_row, parent, false)
        return RoomViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return roomList.size
    }

    override fun onBindViewHolder(holder: RoomViewHolder, position: Int) {


        holder.itemView.setOnClickListener {
            val intent = Intent(holder.itemView.context, SlotActivity::class.java)
            intent.putExtra("roomId", roomList[holder.adapterPosition].id.toString())
            intent.putExtra("room_name", roomList[holder.adapterPosition].name)
            //intent.addFlags( or Intent.FLAG_ACTIVITY_NEW_TASK)
            (holder.itemView.context as Activity).apply {
                startActivity(intent)
                finish()
            }
        }
        holder.itemView.setOnLongClickListener {
            val primaryActionModeCallback = ContextAppbarCallback()
            primaryActionModeCallback.startActionMode(
                holder.itemView,
                R.menu.activity_slot_menu,
                "Room Management"
            )
            return@setOnLongClickListener true
        }

        val roomModel = roomList[position]
        var totalEmployees = 0
        holder.itemView.txt_room_name.text = roomModel.name
        roomModel.slots.map {
            totalEmployees += it.slotDetail.size
        }
        holder.itemView.txt_room_detail.text =
            String.format("%d Slots | %d Employees", roomModel.slots.size, totalEmployees)
    }

    fun updateList(list: List<RoomModel>) {
        this.roomList = list
        notifyDataSetChanged()
    }

}

class RoomViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    init {

    }
}