package com.example.roomassignment.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.roomassignment.R
import com.example.roomassignment.model.EmployeeModel
import com.example.roomassignment.ui.activity.SelectEmployeesActivity
import com.example.roomassignment.ui.adapter.SlotEmployeeAdapter
import com.example.roomassignment.ui.core.BaseFragment
import com.example.roomassignment.ui.viewmodel.SlotEmployeeViewModel
import kotlinx.android.synthetic.main.fragment_slot_employee.*
import javax.inject.Inject


class SlotEmployeeFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var slotEmployeeAdapter = SlotEmployeeAdapter(listOf())
    private lateinit var slotEmployeeViewModel: SlotEmployeeViewModel
    private var bundle: Bundle? = Bundle()
    private var slotId: Int = -1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        slotEmployeeViewModel =
            ViewModelProviders.of(this, viewModelFactory).get(SlotEmployeeViewModel::class.java)



        return inflater.inflate(R.layout.fragment_slot_employee, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        bundle = this.arguments
        slotId = bundle?.getInt("slot_id").toString().toInt()

        slotEmployeeViewModel.getDataWithSlotId(slotId)
        attachObserver()

        fab.setOnClickListener {
            //slotEmployeeViewModel.deleteWithSlotId(slotId)
            val intent = Intent(context, SelectEmployeesActivity::class.java)
            intent.putExtra("slot_id", slotId)
            startActivity(intent)
        }
    }

    private fun attachObserver() {
        slotEmployeeViewModel.slotDetailLiveData.observe(this, Observer {
            it.map { slotDetailSlotMasterEmployeeJoin ->
                slotDetailSlotMasterEmployeeJoin.mapToViewModel()
            }.apply slotDetailModelList@{
                arrayListOf<EmployeeModel>().apply {
                    addAll(this@slotDetailModelList.map { slotDetailModel ->
                        slotDetailModel.employee.first()
                    })
                    slotEmployeeAdapter.updateList(this)
                }

            }
//            it.map {
//                slotEmployeeAdapter.updateList(it.mapToViewModel().employee)
//            }
        })
    }

    private fun setupRecyclerView() {
        slot_employee_recyclerView.layoutManager = LinearLayoutManager(context)
        slot_employee_recyclerView.adapter = slotEmployeeAdapter
    }
}