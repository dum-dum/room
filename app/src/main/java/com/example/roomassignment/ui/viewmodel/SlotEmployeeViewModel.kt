package com.example.roomassignment.ui.viewmodel

import android.util.Log
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Transformations
import com.example.roomassignment.data.slotdetail.SlotDetailEntity
import com.example.roomassignment.data.slotdetail.SlotDetailRepository
import com.example.roomassignment.data.slotdetail.SlotDetailSlotMasterEmployeeJoin
import com.example.roomassignment.ui.core.BaseViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SlotEmployeeViewModel @Inject constructor(private val slotDetailRepository: SlotDetailRepository) :
    BaseViewModel() {
    private val TAG = javaClass.simpleName
    val slotDetailLiveData = MediatorLiveData<List<SlotDetailSlotMasterEmployeeJoin>>()

    fun getData() {
        slotDetailLiveData.addSource(
            Transformations.map(slotDetailRepository.getSlotsDetail()) {
                //                it.map { slotDetailSlotMasterEmployeeJoin -> slotDetailSlotMasterEmployeeJoin.mapToViewModel() }
                slotDetailLiveData.postValue(it)
            }
        ) {
            //slotDetailLiveData.postValue(it)
        }
    }

    fun getDataWithSlotId(slotId: Int) {
        slotDetailLiveData.addSource(
            Transformations.map(slotDetailRepository.getSlotsDetailWithSlotId(slotId)) {
                slotDetailLiveData.postValue(it)
            }
        ) {

        }
    }

    fun deleteWithSlotId(id: Int) {
        Observable.fromCallable { slotDetailRepository.deleteWithSlotId(id) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Log.d(TAG, "Deleted $it data form SlotDetail")
            }.apply { compositeDisposable.add(this) }
    }

    fun insertSlotDetail(slotId: Int, empId: Int) {
        Observable.fromCallable {
            slotDetailRepository.insert(SlotDetailEntity(slotId, empId))
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Log.d(TAG, "Inserted $it")
            }.apply { compositeDisposable.add(this) }
    }
}