package com.example.roomassignment.ui.core

import android.annotation.SuppressLint
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import com.example.roomassignment.R
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.CompositeDisposable


@SuppressLint("Registered")

open class BaseActivity : DaggerAppCompatActivity() {

    private var compositeDisposable = CompositeDisposable()

    override fun onPause() {
        super.onPause()
        compositeDisposable.clear()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    private fun addFragment(
        @IdRes containerId: Int, fragment: Fragment,
        addToBackStack: Boolean = true,
        animate: Boolean = false
    ) {
        supportFragmentManager.beginTransaction().apply {
            if (addToBackStack) {
                addToBackStack(fragment.javaClass.simpleName)
            }
            if (animate) {
                setCustomAnimations(
                    R.anim.slide_from_right,
                    R.anim.slide_to_left,
                    R.anim.slide_from_left,
                    R.anim.slide_to_right
                )
            }
            add(containerId, fragment)
            commit()
        }
    }

    private fun attachFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .attach(fragment)
            .commit()
    }

    fun replaceFragment(
        @IdRes containerId: Int, fragment: Fragment,
        addToBackStack: Boolean = true, animate: Boolean = false

    ) {
        supportFragmentManager.beginTransaction().apply {
            if (addToBackStack) {
                addToBackStack(fragment.javaClass.simpleName)
            }
            if (animate) {
                setCustomAnimations(
                    R.anim.fade_in,
                    R.anim.fade_out,
                    R.anim.fade_in,
                    R.anim.fade_out
                )
            }

            replace(containerId, fragment)
            commit()
        }
    }

    fun addOrAttachFragment(fragment: Fragment, @IdRes containerId: Int) {
        //To prevent IllegalStateException - Fragment already added
        synchronized(this) {
            if (!fragment.isAdded && !fragment.isDetached) {
                addFragment(containerId, fragment, false)
            } else if (fragment.isDetached) {
                attachFragment(fragment)
            }
        }
    }


}