package com.example.roomassignment.ui.activity

import android.os.Bundle
import com.example.roomassignment.R
import com.example.roomassignment.ui.core.BaseNavigationBar
import com.example.roomassignment.ui.fragment.RoomFragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseNavigationBar() {

    private val TAG = this.javaClass.simpleName
    private var roomFragment: RoomFragment = RoomFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.title = "Rooms"


        if (savedInstanceState == null) {
            init()
        }

    }

    private fun init() {
        replaceFragment(R.id.fragment_container_main, roomFragment, false, true)
        navigation.setCheckedItem(R.id.nav_room)
        navigation.menu.findItem(R.id.nav_room).isChecked = true
    }

}
