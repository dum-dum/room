package com.example.roomassignment.ui.viewmodel

import android.util.Log
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Transformations
import com.example.roomassignment.data.room.RoomRepository
import com.example.roomassignment.data.slotmaster.SlotMasterEntity
import com.example.roomassignment.data.slotmaster.SlotMasterRepository
import com.example.roomassignment.model.SlotMasterModel
import com.example.roomassignment.ui.core.BaseViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.joda.time.LocalTime
import javax.inject.Inject

class SlotViewModel @Inject constructor(
    private val slotMasterRepository: SlotMasterRepository,
    private val roomRepository: RoomRepository
) :
    BaseViewModel() {
    private val TAG = RoomViewModel::class.java.simpleName
    val slotLiveData = MediatorLiveData<List<SlotMasterModel>>()

    fun getData() {
        slotLiveData.addSource(
            Transformations.map(slotMasterRepository.getSlots()) {
                it.map { slotMasterSlotDetailJoin ->
                    slotMasterSlotDetailJoin.mapToViewModel()
                }
            }
        ) {
            slotLiveData.postValue(it)
        }
    }

    fun getDataByRoomId(id: Int) {
        slotLiveData.addSource(
            Transformations.map(slotMasterRepository.getSlotsByRoomId(id)) {
                it.map { slotMasterSlotDetailRoomJoin ->
                    slotMasterSlotDetailRoomJoin.mapToViewModel()
                }
            }
        ) {
            slotLiveData.postValue(it)
        }
    }

    fun insertData(name: String, startTime: LocalTime, endTime: LocalTime, roomId: Int = -1) {
        Observable.fromCallable {
            slotMasterRepository.insert(
                SlotMasterEntity(
                    null,
                    name,
                    startTime,
                    endTime,
                    roomId
                )
            )
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Log.d(TAG, "Inserted $it")
            }.apply { compositeDisposable.add(this) }
    }

    fun deleteRoomWithRoomId(roomId: Int) {
        Observable.fromCallable {
            roomRepository.deleteRoomById(roomId)
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Log.d(TAG, "Deleted Room: $it")
            }.apply { compositeDisposable.add(this) }
    }

}