package com.example.roomassignment.ui.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.roomassignment.R
import kotlinx.android.synthetic.main.fragment_slot_about.*


class SlotAboutFragment : Fragment() {

    private var bundle: Bundle? = Bundle()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_slot_about, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bundle = this.arguments
        txt_start_time.text = bundle?.getString("start_time")
        txt_end_time.text = bundle?.getString("end_time")
    }

}
