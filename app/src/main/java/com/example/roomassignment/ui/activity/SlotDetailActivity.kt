package com.example.roomassignment.ui.activity

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.roomassignment.R
import com.example.roomassignment.ui.adapter.SlotDetailPageAdapter
import com.example.roomassignment.ui.core.BaseActivity
import com.example.roomassignment.ui.fragment.SlotAboutFragment
import com.example.roomassignment.ui.fragment.SlotEmployeeFragment
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_slot_detail.*

class SlotDetailActivity : BaseActivity() {

    private val slotEmployeeFragment = SlotEmployeeFragment()
    private val slotAboutFragment: Fragment = SlotAboutFragment()
    private val fragmentArray = arrayOf(slotEmployeeFragment, slotAboutFragment)
    private var slotId: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_slot_detail)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = intent.extras?.getString("slot_name")

        val slotMasterBundle = Bundle()
        slotId = intent.extras?.getString("slot_id").toString().toInt()
        slotMasterBundle.putInt("slot_id", slotId)
        slotEmployeeFragment.arguments = slotMasterBundle

        val aboutBundle = Bundle()
        aboutBundle.putString("start_time", intent.extras?.getString("start_time"))
        aboutBundle.putString("end_time", intent.extras?.getString("end_time"))
        slotAboutFragment.arguments = aboutBundle

        setupTabs()
    }

    private fun setupTabs() {
        val tabAdapter = SlotDetailPageAdapter(supportFragmentManager, fragmentArray)
        view_pager.adapter = tabAdapter
        view_pager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tab_layout))
        tab_layout.setupWithViewPager(view_pager)
    }

    override fun onSupportNavigateUp(): Boolean {
        super.onBackPressed()
        return super.onSupportNavigateUp()
    }
}
