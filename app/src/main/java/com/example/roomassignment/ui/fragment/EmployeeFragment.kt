package com.example.roomassignment.ui.fragment


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.roomassignment.R
import com.example.roomassignment.ui.adapter.EmployeeAdapter
import com.example.roomassignment.ui.core.BaseFragment
import com.example.roomassignment.ui.viewmodel.EmployeeViewModel
import kotlinx.android.synthetic.main.add_employee_dialog.view.*
import kotlinx.android.synthetic.main.fragment_employee.*
import javax.inject.Inject


class EmployeeFragment : BaseFragment() {


    private val TAG = javaClass.simpleName

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var employeeViewModel: EmployeeViewModel
    private lateinit var employeeAdapter: EmployeeAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var dialogView: View
    private lateinit var builder: AlertDialog.Builder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        employeeViewModel =
            ViewModelProviders.of(this, viewModelFactory)[EmployeeViewModel::class.java]
        employeeAdapter = EmployeeAdapter(arrayListOf())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val rootView = inflater.inflate(R.layout.fragment_employee, container, false)
        recyclerView = rootView.findViewById(R.id.employee_recyclerView)
        employeeViewModel.getData()
        attachObserver()
        setupRecyclerView()
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fab.setOnClickListener {
            addEmployee()
        }
    }

    private fun attachObserver() {
        employeeViewModel.employeeLiveData.observe(this, Observer {
            employeeAdapter.updateList(it)
        })
    }

    private fun setupRecyclerView() {
        recyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerView.adapter = employeeAdapter
    }

    private fun addEmployee() {
        dialogView = LayoutInflater.from(activity).inflate(R.layout.add_employee_dialog, null)
        builder = AlertDialog.Builder(activity!!)
            .setView(dialogView)
            .setTitle("Add Employee")
        val alertDialog = builder.show()
        dialogView.button_add_employee.setOnClickListener {
            if (!dialogView.dialog_employee_name.text.isNullOrBlank()) {
                Log.d(TAG, dialogView.dialog_employee_name.text.toString())
                employeeViewModel.insertEmployeeData(dialogView.dialog_employee_name.text.toString())
                alertDialog.dismiss()
            } else {
                dialogView.dialog_employee_name.error = "Please enter Employee Name"
            }

        }
    }


}
