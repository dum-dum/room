package com.example.roomassignment.ui.activity

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.roomassignment.R
import com.example.roomassignment.model.EmployeeModel
import com.example.roomassignment.ui.adapter.SelectEmployeeAdapter
import com.example.roomassignment.ui.core.BaseActivity
import com.example.roomassignment.ui.viewmodel.EmployeeViewModel
import kotlinx.android.synthetic.main.activity_select_employees.*
import javax.inject.Inject

class SelectEmployeesActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var employeeViewModel: EmployeeViewModel
    private var selectEmployeeAdapter = SelectEmployeeAdapter(arrayListOf())
    { i: Int, employeeModel: EmployeeModel, b: Boolean ->

        Log.e(
            javaClass.simpleName,
            "Position: $i, Employee detail: $employeeModel, Employees isSelected: ${employeeModel.isSelected} and isChecked: $b"
        )
        if (b) {
            Log.d(javaClass.simpleName, "Slot Id: $slotId")
            employeeViewModel.insertSlotDetail(slotId, employeeModel.id)
        } else {
            employeeViewModel.deleteSlotDetailWithEmployeeId(employeeModel.id)
        }
    }
    private var slotId: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_employees)
        supportActionBar?.title = "Select Employees"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        employeeViewModel =
            ViewModelProviders.of(this, viewModelFactory)[EmployeeViewModel::class.java]

        slotId = (intent.extras?.getInt("slot_id") as Int)

        employeeViewModel.getData()
        attachObserver()
        setupRecyclerView()
    }

    private fun attachObserver() {

        employeeViewModel.employeeLiveData.observe(this, Observer {
            it.apply {
                this.forEach { employeeModel ->
                    employeeModel.slotDetail.map { slotDetailModel ->
                        if (slotDetailModel.empId == employeeModel.id && slotDetailModel.slotId == slotId) {
                            employeeModel.isSelected = true
                        }
                    }
                }
            }
            Log.e(javaClass.simpleName, "Employees from Observable $it")
            selectEmployeeAdapter.updateList(it)
        })
    }

    private fun setupRecyclerView() {
        select_employee_recyclerView.layoutManager = LinearLayoutManager(this)
        select_employee_recyclerView.adapter = selectEmployeeAdapter
        //selectEmployeeAdapter.updateList(allEmployees,selectionEmployees)
    }

    override fun onSupportNavigateUp(): Boolean {
        super.onBackPressed()
        return super.onSupportNavigateUp()
    }

}
