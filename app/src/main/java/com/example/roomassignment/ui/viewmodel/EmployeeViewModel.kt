package com.example.roomassignment.ui.viewmodel

import android.util.Log
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Transformations
import com.example.roomassignment.data.employee.EmployeeEntity
import com.example.roomassignment.data.employee.EmployeeRepository
import com.example.roomassignment.data.slotdetail.SlotDetailEntity
import com.example.roomassignment.data.slotdetail.SlotDetailRepository
import com.example.roomassignment.model.EmployeeModel
import com.example.roomassignment.ui.core.BaseViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class EmployeeViewModel @Inject constructor(
    private val employeeRepository: EmployeeRepository,
    private val slotDetailRepository: SlotDetailRepository
) :
    BaseViewModel() {
    private val TAG = EmployeeViewModel::class.java.simpleName
    val employeeLiveData = MediatorLiveData<List<EmployeeModel>>()

    fun getData() {
        employeeLiveData.addSource(
            Transformations.map(employeeRepository.getEmployees()) { it ->
                it.map {
                    it.mapToViewModel()
                }
            }
        ) {
            employeeLiveData.postValue(it)
        }
    }

    fun insertEmployeeData(name: String) {
        Observable.fromCallable { employeeRepository.insert(EmployeeEntity(name)) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Log.d(TAG, "Inserted $it")
            }.apply { compositeDisposable.add(this) }
    }

    fun insertSlotDetail(slotId: Int, empId: Int) {
        Observable.fromCallable {
            slotDetailRepository.insert(SlotDetailEntity(slotId, empId))
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Log.d(TAG, "Inserted $it")
            }.apply { compositeDisposable.add(this) }
    }

    fun deleteSlotDetailWithEmployeeId(empId: Int) {

        Observable.fromCallable {
            slotDetailRepository.deleteWithEmpId(empId)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Log.d(TAG, "Deleted $it Employees from SlotDetail")
            }.apply { compositeDisposable.add(this) }

    }
}