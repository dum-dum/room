package com.example.roomassignment.ui.adapter


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.roomassignment.R
import com.example.roomassignment.model.EmployeeModel
import kotlinx.android.synthetic.main.select_employee_row.view.*


class SelectEmployeeAdapter(
    private var employeeList: List<EmployeeModel>,
    val callback: (Int, EmployeeModel, Boolean) -> Unit
) :
    RecyclerView.Adapter<SelectEmployeeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectEmployeeViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.select_employee_row, parent, false)


        return SelectEmployeeViewHolder(itemView)
    }

    override fun getItemCount(): Int = employeeList.size

    override fun onBindViewHolder(holder: SelectEmployeeViewHolder, position: Int) {
        val employeeModel = employeeList[position]

        holder.itemView.txt_employee_name.text = employeeModel.name

        holder.itemView.isSelected = employeeModel.isSelected

        holder.itemView.setOnClickListener {
            employeeModel.isSelected = !employeeModel.isSelected
            holder.itemView.isSelected = employeeModel.isSelected
            callback(position, employeeModel, employeeModel.isSelected)
            /*Log.e(javaClass.simpleName, holder.itemView.isSelected.toString())*/
            Toast.makeText(holder.itemView.context, "Position $position", Toast.LENGTH_SHORT)
                .show()
        }

    }

    fun updateList(employees: List<EmployeeModel>) {
        this.employeeList = employees
        notifyDataSetChanged()
    }

}

class SelectEmployeeViewHolder(view: View) : RecyclerView.ViewHolder(view)