package com.example.roomassignment.ui.utils

import android.view.MenuItem

interface OnActionItemClickListener {
    fun onActionItemClick(item: MenuItem)
}
