package com.example.roomassignment.ui.adapter


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.roomassignment.R
import com.example.roomassignment.model.EmployeeModel
import kotlinx.android.synthetic.main.employee_list_row.view.*

class SlotEmployeeAdapter(private var employeeList: List<EmployeeModel>) :
    RecyclerView.Adapter<SlotEmployeeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SlotEmployeeViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.employee_list_row, parent, false)
        return SlotEmployeeViewHolder(itemView)
    }

    override fun getItemCount(): Int = employeeList.size

    override fun onBindViewHolder(holder: SlotEmployeeViewHolder, position: Int) {
        val list = employeeList[position]
        holder.itemView.txt_employee_name.text = list.name
    }

    fun updateList(list: List<EmployeeModel>) {
        this.employeeList = list
        notifyDataSetChanged()
    }

}

class SlotEmployeeViewHolder(view: View) : RecyclerView.ViewHolder(view)