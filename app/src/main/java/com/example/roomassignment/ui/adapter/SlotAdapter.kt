package com.example.roomassignment.ui.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.roomassignment.R
import com.example.roomassignment.model.SlotMasterModel
import com.example.roomassignment.ui.activity.SlotDetailActivity
import com.example.roomassignment.ui.core.BaseActivity
import kotlinx.android.synthetic.main.slot_list_row.view.*

class SlotAdapter(private var slotList: List<SlotMasterModel>) :
    RecyclerView.Adapter<SlotViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SlotViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.slot_list_row, parent, false)
        return SlotViewHolder(itemView)
    }

    override fun getItemCount(): Int = slotList.size


    override fun onBindViewHolder(holder: SlotViewHolder, position: Int) {
        val list = slotList[position]

        holder.itemView.setOnClickListener {
            val intent = Intent(holder.itemView.context, SlotDetailActivity::class.java)
            intent.putExtra("slot_id", list.id.toString())
            intent.putExtra("roomId", list.roomId.toString())
            intent.putExtra("slot_name", list.name)
            intent.putExtra("start_time", list.startTime)
            intent.putExtra("end_time", list.endTime)
            (holder.itemView.context as BaseActivity).apply {
                startActivity(intent)
            }
        }

        holder.itemView.txt_slot_name.text = list.name
        holder.itemView.txt_slot_time.text = String.format(
            "%s - %s",
            list.startTime.toString("hh:mm"),
            list.endTime.toString("hh:mm")
        )
        holder.itemView.txt_slot_detail.text = String.format("%d Employee", list.slotDetail.size)


    }

    fun updateList(list: List<SlotMasterModel>) {
        this.slotList = list
        notifyDataSetChanged()
    }

}

class SlotViewHolder(view: View) : RecyclerView.ViewHolder(view)