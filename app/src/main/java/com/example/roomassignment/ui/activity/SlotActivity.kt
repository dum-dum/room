package com.example.roomassignment.ui.activity

import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.roomassignment.R
import com.example.roomassignment.ui.adapter.SlotAdapter
import com.example.roomassignment.ui.core.BaseNavigationBar
import com.example.roomassignment.ui.viewmodel.SlotViewModel
import kotlinx.android.synthetic.main.activity_slots.*
import kotlinx.android.synthetic.main.add_slot_dialog.view.*
import kotlinx.android.synthetic.main.content_main.*
import org.joda.time.LocalTime
import java.util.*
import javax.inject.Inject


class SlotActivity : BaseNavigationBar() {
    private val TAG = javaClass.simpleName

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var slotViewModel: SlotViewModel
    private var slotAdapter: SlotAdapter = SlotAdapter(arrayListOf())
    private lateinit var recyclerView: RecyclerView
    private lateinit var dialogView: View
    private lateinit var builder: AlertDialog.Builder
    private lateinit var inflater: LayoutInflater
    private lateinit var rootLayout: View

    private var roomId: Int = -1

    private val currentTime: Calendar = Calendar.getInstance()
    private val hour = currentTime.get(Calendar.HOUR_OF_DAY)
    private val minute = currentTime.get(Calendar.MINUTE)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        roomId = intent.extras?.getString("roomId").toString().toInt()
        supportActionBar?.title = intent.extras?.getString("room_name")
        slotViewModel = ViewModelProviders.of(this, viewModelFactory)[SlotViewModel::class.java]
        init()
        slotViewModel.getDataByRoomId(roomId)
        attachObserver()
        setupRecyclerView()
        fab.setOnClickListener {
            addSlot()
        }


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_slot_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_remove_room -> {
                slotViewModel.deleteRoomWithRoomId(roomId)
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    private fun init() {
        inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        rootLayout = inflater.inflate(R.layout.activity_slots, fragment_container_main)
    }

    private fun setupRecyclerView() {
        recyclerView = findViewById(R.id.recyclerview_slots)
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView.adapter = slotAdapter
    }

    private fun attachObserver() {
        slotViewModel.slotLiveData.observe(this, Observer {
            slotAdapter.updateList(it)
        })
    }

    private fun addSlot() {
        var endTime = LocalTime()
        var startTime = LocalTime()
        dialogView = LayoutInflater.from(this).inflate(R.layout.add_slot_dialog, null)
        builder = AlertDialog.Builder(this)
            .setView(dialogView)
            .setTitle("Add Slot")
        val alertDialog = builder.show()
        dialogView.et_start_time.setOnClickListener {
            val timePicker = TimePickerDialog(
                this, TimePickerDialog.OnTimeSetListener { _, selectedHour, selectedMinute ->
                    startTime =
                        LocalTime().withHourOfDay(selectedHour).withMinuteOfHour(selectedMinute)
                    dialogView.et_start_time.setText(
                        String.format("%02d:%02d", selectedHour, selectedMinute)
                    )
                }, hour, minute, false
            )
            timePicker.setTitle("Select Start Time")
            timePicker.show()
        }

        dialogView.et_end_time.setOnClickListener {
            val timePicker = TimePickerDialog(
                this, TimePickerDialog.OnTimeSetListener { _, selectedHour, selectedMinute ->

                    endTime =
                        LocalTime().withHourOfDay(selectedHour).withMinuteOfHour(selectedMinute)
                    dialogView.et_end_time.setText(
                        String.format("%02d:%02d", selectedHour, selectedMinute)
                    )
                }, hour, minute, false
            )
            timePicker.setTitle("Select End Time")
            timePicker.show()
        }

        dialogView.button_add_slot.setOnClickListener {
            Log.d(TAG, dialogView.et_slot_name.text.toString())
            slotViewModel.insertData(
                dialogView.et_slot_name.text.toString(),
                startTime,
                endTime,
                roomId
            )
            alertDialog.dismiss()
        }

    }
}
