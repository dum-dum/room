package com.example.roomassignment.ui.fragment.room.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.roomassignment.R
import com.example.roomassignment.data.room.RoomEntity
import com.example.roomassignment.model.RoomModel
import kotlinx.android.synthetic.main.room_list_row.view.*

class RoomAdapter(private var roomList: List<RoomModel>) : RecyclerView.Adapter<RoomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoomViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.room_list_row, parent, false)
        return RoomViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return roomList.size
    }

    override fun onBindViewHolder(holder: RoomViewHolder, position: Int) {
        val list = roomList[position]
        holder.itemView.txt_room_name.text = list.name
    }

    fun updateList(list:List<RoomModel>){
        this.roomList = list
        notifyDataSetChanged()
    }
}

class RoomViewHolder(view: View) : RecyclerView.ViewHolder(view)