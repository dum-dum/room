package com.example.roomassignment.ui.core

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import com.example.roomassignment.R
import com.example.roomassignment.ui.fragment.EmployeeFragment
import com.example.roomassignment.ui.fragment.RoomFragment
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*

@SuppressLint("Registered")
open class BaseNavigationBar : BaseActivity(),
    NavigationView.OnNavigationItemSelectedListener {

    private var roomFragment: RoomFragment = RoomFragment()
    private var employeeFragment: EmployeeFragment = EmployeeFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        navigation.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_room -> {
                supportActionBar?.title = "Room"
                navigation.setCheckedItem(R.id.nav_room)
                navigation.menu.findItem(R.id.nav_room).isChecked = true
                fragment_container_main.removeAllViews()
                replaceFragment(R.id.fragment_container_main, roomFragment, false, true)
            }
            R.id.nav_employee -> {
                supportActionBar?.title = "Employee"
                navigation.setCheckedItem(R.id.nav_employee)
                navigation.menu.findItem(R.id.nav_employee).isChecked = true
                fragment_container_main.removeAllViews()
                replaceFragment(R.id.fragment_container_main, employeeFragment, false, true)
            }
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}