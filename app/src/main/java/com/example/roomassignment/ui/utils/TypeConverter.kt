package com.example.roomassignment.ui.utils

import androidx.room.TypeConverter
import org.joda.time.LocalTime


class TypeConverters {

    @TypeConverter
    fun toOffsetDateTime(value: Int): LocalTime {
        return LocalTime().withMillisOfDay(value)
    }

    @TypeConverter
    fun fromOffsetDateTime(date: LocalTime): Int {
        return date.millisOfDay
    }
}
