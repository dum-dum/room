package com.example.roomassignment.ui.viewmodel

import android.util.Log
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Transformations
import com.example.roomassignment.data.room.RoomEntity
import com.example.roomassignment.data.room.RoomRepository
import com.example.roomassignment.model.RoomModel
import com.example.roomassignment.ui.core.BaseViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class RoomViewModel @Inject constructor(private val roomRepository: RoomRepository) :
    BaseViewModel() {
    private val TAG = RoomViewModel::class.java.simpleName
    val roomLiveData = MediatorLiveData<List<RoomModel>>()

    fun getData() {
        roomLiveData.addSource(
            Transformations.map(roomRepository.getRooms()) {
                it.map { roomSlotJoin ->
                    with(roomSlotJoin.roomEntity) {
                        RoomModel(id ?: -1, name, roomSlotJoin.slots.map { slotMasterEntity ->
                            slotMasterEntity.mapToViewModel()
                        })
                    }
                }
            }) {
            roomLiveData.postValue(it)
        }
    }

    fun getDataByUserId(id: Int) {
        roomLiveData.addSource(
            Transformations.map(roomRepository.getRoomById(id)) {
                it.map {
                    with(it.roomEntity) {
                        RoomModel(id ?: -1, name, it.slots.map { slotMasterEntity ->
                            slotMasterEntity.mapToViewModel()
                        })
                    }
                }
            }
        ) {
            roomLiveData.postValue(it)
        }
    }

    fun insertData(name: String) {
        Observable.fromCallable { roomRepository.insert(RoomEntity(name)) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Log.d(TAG, "Inserted $it")
            }.apply { compositeDisposable.add(this) }

    }
}
