package com.example.roomassignment.ui.fragment.room

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.roomassignment.R
import com.example.roomassignment.ui.core.BaseFragment
import com.example.roomassignment.ui.fragment.room.adapter.RoomAdapter
import com.example.roomassignment.ui.viewmodel.RoomViewModel
import kotlinx.android.synthetic.main.fragment_room.*
import javax.inject.Inject


class RoomFragment : BaseFragment() {
    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var roomViewModel: RoomViewModel
    private lateinit var roomAdapter: RoomAdapter
    private lateinit var recyclerView: RecyclerView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        roomViewModel = ViewModelProviders.of(this,viewModelFactory)[RoomViewModel::class.java]
        roomAdapter = RoomAdapter(arrayListOf())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        val rootView = inflater.inflate(R.layout.fragment_room, container, false)
        recyclerView = rootView.findViewById(R.id.recyclerview_room)
        roomViewModel.getData()
        attachObserver()
        setupRecyclerView()

        return rootView
    }
    private fun attachObserver() {
        roomViewModel.roomLiveData.observe(this, Observer {
            roomAdapter.updateList(it)
        })
    }
    private fun setupRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
        recyclerView.adapter = roomAdapter
    }

}
